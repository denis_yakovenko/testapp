app
    .factory('AuthService', function(APP_CONFIG, UserService, $state){
        return{
            login:function(auth){
                if(auth.name === APP_CONFIG.admin_name && auth.pwd === APP_CONFIG.admin_pwd){
                    auth.isAdmin = true;
                    return UserService.setCurrentUser(auth);
                }
                var user_list = UserService.getUserList();
                if(user_list.length === 0)
                    return false;
                var user;
                angular.forEach(user_list, function(v){
                    if(v.name.toLowerCase() === auth.name.toLowerCase() && v.pwd === auth.pwd)
                        user = v;
                });

                if(typeof user !== 'undefined')
                    return UserService.setCurrentUser(auth);

                return false;
            },
            logout: function(){
                UserService.clearCurrentUser();
                $state.go('login');
            }
        }
    })
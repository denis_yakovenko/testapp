app
    .factory('UserService', function(APP_CONFIG){
        var current_user = localStorage.getItem(APP_CONFIG.app_name + 'current_user');
        var last_id = localStorage.getItem(APP_CONFIG.app_name + 'last_id') || 0;
        var user_list;
        function incrLastId(){
            last_id++;
            localStorage.setItem(APP_CONFIG.app_name + 'last_id', last_id);
        }

        return{
            isLoggedIn: function(){
                return current_user;
            },
            getLastId: function(){
                return last_id;
            },
            clearLastId: function(){
                last_id = 0;
                localStorage.removeItem(APP_CONFIG.app_name + 'last_id')
            },
            changeUser: function (user){
                angular.forEach(user_list, function(v){
                    if(v.id === user.id){
                        v.name = user.name;
                        v.pwd = user.pwd;
                    }
                })
            },
            isUserExist: function(user){
                if(user.name === APP_CONFIG.admin_name)
                    return true;

                var found = false;
                angular.forEach(user_list, function(v){
                    if(v.name.toLowerCase() === user.name.toLowerCase() && v.id !== user.id)
                        found = true;
                });
                return found;
            },
            setCurrentUser: function (user){
                user = JSON.stringify(user);
                localStorage.setItem(APP_CONFIG.app_name + 'current_user', user);
                current_user = user;
                return user;
            },
            getUserList: function(){
                user_list = JSON.parse(localStorage.getItem(APP_CONFIG.app_name + 'user_list'));
                return user_list;
            },
            getCurrentUser: function(){
                return JSON.parse(current_user);
            },
            clearCurrentUser: function(){
                localStorage.removeItem(APP_CONFIG.app_name + 'current_user');
                current_user = false;
            },
            submitUserList: function(_user_list, create){
                user_list = _user_list;
                localStorage.setItem(APP_CONFIG.app_name + 'user_list', JSON.stringify(user_list));
                if(create)
                    incrLastId();
            }
        }
    });
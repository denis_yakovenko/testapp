app
    .config(function ($stateProvider, $urlRouterProvider) {

        $stateProvider

            .state('start', {
                url: '/',
                resolve: {
                    isLogged: function(UserService, $state, $timeout){
                        $timeout(function(){
                            UserService.isLoggedIn() ? $state.go('user-list') : $state.go('login');
                        })
                    }
                }
            })
            .state('login', {
                url: '/login',
                template: '<login></login>'
            })
            .state('user-list', {
                url: '/user-list',
                template: '<user-list></user-list>'
            });

        $urlRouterProvider.otherwise('/');
    });
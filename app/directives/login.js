app
    .directive('login', function (AuthService, $state) {
        return {
            restrict: 'E',
            scope: {},
            templateUrl: 'views/login.html',
            link: function(scope, el){
                scope.auth = {};
                scope.login = function($form){
                    scope.error = false;
                    if(!$form.$valid)
                        return false;
                    AuthService.login(scope.auth) ? $state.go('user-list') : scope.error = true;
                }
            }
        }
    })
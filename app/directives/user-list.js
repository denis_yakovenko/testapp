app
    .directive('userList', function(UserService, AuthService, $state){
        return {
            restrict: 'E',
            scope: {},
            templateUrl: 'views/user-list.html',
            link: function(scope){
                scope.user = {};
                scope.current_user = UserService.getCurrentUser();

                if(!scope.current_user)
                    $state.go('login');

                scope.user_list = UserService.getUserList() || [];
                scope.logout = function(){
                    AuthService.logout();
                };

                scope.submitUser = function($form, user){
                    scope.error = false;
                    if(!$form.$valid)
                        return false;
                    var create = false;
                    if(typeof user.id === 'undefined'){
                        if(UserService.isUserExist(user) === true){
                            scope.error = true;
                            return false;
                        }
                        user.id = UserService.getLastId();
                        create = true;
                        scope.user_list.push(user);
                    } else {
                        if(UserService.isUserExist(user) === true){
                            scope.error = true;
                            return false;
                        }
                        UserService.changeUser(user);
                    }
                    scope.user = {};
                    UserService.submitUserList(scope.user_list, create);
                };


                scope.deleteUser = function ($index) {
                    scope.user_list.splice($index, 1);
                    UserService.submitUserList(scope.user_list);
                    if($index === 0)
                        UserService.clearLastId();
                };
                scope.editUser = function (item) {
                    scope.user = angular.copy(item);
                };

            }
        }
    })
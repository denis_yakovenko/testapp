var
    gulp = require('gulp'),
    plugins = require("gulp-load-plugins")({lazy: false}),
    concat = require('gulp-concat'),
    less = require('gulp-less'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify'),
    ngAnnotate = require('gulp-ng-annotate'),
    minifyCSS = require('gulp-minify-css'),
    path = require('path'),
    clean = require('gulp-clean'),

    config = {
        SCRIPTS: [
            "./app/app.js",
            "./app/config/**/*.js",
            "./app/controllers/**/*.js",
            "./app/services/**/*.js",
            "./app/directives/**/*.js"
        ],
        BOWER_SCRIPTS: [
            './bower_components/jquery/dist/jquery.min.js',     // NEED for bootstrap
            './bower_components/angular/angular.min.js',
            './bower_components/angular-ui-router/release/angular-ui-router.min.js',
            './bower_components/bootstrap/dist/js/bootstrap.min.js',
        ],
        BOWER_CSS: [
            './bower_components/bootstrap/dist/css/bootstrap.min.css'
        ],
        BOWER_FONTS: [
            './bower_components/bootstrap/dist/fonts/**/*.*'
        ]
    };

gulp.task('scripts_min', function () {
    gulp.src(config.SCRIPTS)
        .pipe(sourcemaps.init())
        .pipe(concat('app.min.js'))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./www/assets/js'));
});

gulp.task('scripts_dev', function () {
    gulp.src(config.SCRIPTS)
        .pipe(sourcemaps.init())
        .pipe(concat('app.min.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./www/assets/js'));
});

gulp.task('concat_css', function(){
    gulp.src(['./app/assets/css/**/*.css'])
        .pipe(concat('app.min.css'))
        .pipe(gulp.dest('./www/assets/css'));
})

gulp.task('vendorBOWER', function () {
    gulp.src(config.BOWER_SCRIPTS)
        .pipe(plugins.concat('bower-components.min.js'))
        .pipe(gulp.dest('./www/assets/js'));
    gulp.src(config.BOWER_CSS)
        .pipe(plugins.concat('bower-components.min.css'))
        .pipe(gulp.dest('./www/assets/css'));
    gulp.src(config.BOWER_FONTS)
        .pipe(gulp.dest('./www/assets/fonts'));
});

gulp.task('copy_index', function () {
    gulp.src('./app/index.html')
        .pipe(gulp.dest('./www'));
});

gulp.task('templates', function () {
    gulp.src(['!./app/index.html',
        './app/**/**/*.html'])
        .pipe(plugins.angularTemplatecache('templates.js', {standalone: true}))
        .pipe(gulp.dest('./www/assets/js'));
});


gulp.task('watch_min', function () {
    gulp.watch([
        'www/assets/**/*.html',
        'www/assets/**/*.js',
        'www/assets/**/*.css'
    ], function (event) {
        return gulp.src(event.path)
            .pipe(plugins.connect.reload());
    });
    gulp.watch(['./www/assets/js/**/*.js'], ['scripts_min']);
    gulp.watch('./www/index.html', ['copy_index']);
    gulp.watch('./www/assets/css/**/*.css', ['css']);
});
gulp.task('watch_dev', function () {
    gulp.watch([
        './www/index.html',
        './www/assets/js/*.js',
        './www/assets/css/*.css'
    ], function (event) {
        return gulp.src(event.path)
            .pipe(plugins.connect.reload());
    });
    gulp.watch(['./app/**/*.js'], ['scripts_dev']);
    gulp.watch('./app/index.html', ['copy_index']);
    gulp.watch('./app/assets/css/**/*.css', ['concat_css']);
    gulp.watch('./app/views/**/*.html', ['templates']);
});

gulp.task('copy-assets', function () {
    gulp.src('./app/assets/fonts/**')
        .pipe(gulp.dest('./www/assets/fonts'));
    gulp.src('./app/assets/img/**/*.*')
        .pipe(gulp.dest('./www/assets/img'));
});

gulp.task('connect', plugins.connect.server({
    root: ['www'],
    port: 9000,
    livereload: true
}));

gulp.task('clean', function () {
    return gulp.src('./assets', {read: false})
        .pipe(clean({force: true}));
});


gulp.task('default', ['connect', 'scripts_dev', 'copy_index', 'concat_css', 'templates', 'copy-assets', 'vendorBOWER', 'watch_dev']);
gulp.task('build', ['scripts_min', 'templates', 'copy_index', 'concat_css', 'copy-assets', 'vendorBOWER']);